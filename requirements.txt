numpy==1.20
Pillow
matplotlib
jupyter
scikit-image
pandas
python-Levenshtein
librosa
